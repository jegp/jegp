var WIR = (function() {

  // The impress API
  var api = impress();
  
  var steps = document.getElementsByClassName("step");
  var list  = document.getElementById("TOC");
  for (var i = 1; i < steps.length; i += 1) {
    try {
      var step = steps[i];
      var item = document.createElement("li");
      var link = document.createElement("a");
      link.href = "#step-" + (i + 1);
      link.innerHTML = step.getElementsByTagName("h1")[0].innerHTML;
      item.appendChild(link);
      list.appendChild(item);
    } catch (e) {}
  }
  
  // Define WILL IT RUN
  var wirs = document.getElementsByClassName("wir");  

  // Callback functions
  var editFunction = function(field, input, code) {
    var textarea = document.createElement("textarea");
    textarea.style.height = input.offsetHeight - 2 + "px";
    textarea.style.width = input.offsetWidth - 2 + "px";
    textarea.value = code.innerHTML;
    
    var blurFunction = function() {
      try {
        input.innerHTML = hljs.highlight("javascript", textarea.value).value;
        code.innerHTML = textarea.value;
        field.replaceChild(input, textarea);
        textarea.onblur = null;
        field.onclick = focusFunction;
      } catch (e) {}
    }
    
    var focusFunction = function() {
      field.replaceChild(textarea, input);
      textarea.focus();
      field.onclick = null;
      textarea.onblur = blurFunction;
    }
    
    return focusFunction;
  }
  var runFunction = function(input, output) {
    return function() { 
      try {
        output.className = "success";
        output.innerHTML = eval(input.innerHTML); 
      } catch (e) {
        output.className = "error";
        output.innerHTML = e;
      }
    }
  }
  
  // Apply run button
  for (var i = 0; i < wirs.length; i += 1) {
    var wir = wirs[i];
    
    // Get the language
    var language = "javascript";
    for (attr in wir.attributes) {
      if (attr["name"] && attr["name"] === "data-lang") {
        language = attr["value"];
      }
    }
    
    // Create fields
    var runButton = document.createElement("button");
    runButton.innerHTML = "Will it run?";
    var preField = document.createElement("pre");
    var inputCode = document.createElement("span");
    var inputField = document.createElement("code");
    var outputCode = document.createElement("code");
    outputCode.className = "output"
    preField.className = language;
    inputCode.innerHTML  = wir.innerHTML.trim();
    inputField.innerHTML = wir.innerHTML.trim();
    wir.innerHTML = "";
    
    // Insert
    preField.appendChild(inputField);
    preField.appendChild(outputCode);
    wir.appendChild(preField);
    wir.appendChild(runButton); 
    
    // Setup listeners
    runButton.onclick = runFunction(inputCode, outputCode);
    preField.onclick = editFunction(preField, inputField, inputCode);
  }
  
  return {
  };
}());